// This library allows us to use the DHT11 Sensor
#include <DHT_U.h>

//This library allows us to use the ssd1306 128x32 display
#include <Adafruit_SSD1306.h>

#define SCREEN_ADDRESS 0x3C ///< See datasheet for Address; 0x3D for 128x64, 0x3C for 128x32
Adafruit_SSD1306 display(128, 32, &Wire, -1);
#define DHTPIN DD6
#define DHTTYPE    DHT11

DHT_Unified dht(DHTPIN, DHTTYPE);

uint32_t delayMS;

void setup() {
  Serial.begin(9600);
  // Initialize device.
  dht.begin();
  Serial.println(F("DHTxx Unified Sensor Example"));
  // Print temperature sensor details.
  sensor_t sensor;
  dht.temperature().getSensor(&sensor);
  // Set delay between sensor readings based on sensor details.
  delayMS = sensor.min_delay / 1000;

  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if(!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
    Serial.println(F("SSD1306 allocation failed"));
    for(;;); // Don't proceed, loop forever
  }

  // Clear the buffer
  display.clearDisplay();

  

}

bool activeLight = false;

void loop() {
  //thermoLoop();
  drawLoadingScreen();
}

void thermoLoop(){
  drawLoadingScreen();
  drawLoadingScreen();
  for(;;){
    // Delay between measurements.
    
    display.clearDisplay();
    display.setTextSize(2);             // Normal 2:1 pixel scale
    display.setTextColor(SSD1306_WHITE);        // Draw white text
    display.setCursor(0,0);             // Start at top-left corner

    // Get temperature event and print its value.
    sensors_event_t event;
    dht.temperature().getEvent(&event);
    if (isnan(event.temperature)) {
      display.println(F("Error reading temperature!"));
    }
    else {
      display.print(F("T:"));
      display.print(event.temperature*9/5 + 32);
      display.print(F("F"));
      if(activeLight){
        display.println(F(" *"));
      }else{
        display.println(F(" O"));
      }
    }
    // Get humidity event and print its value.
    dht.humidity().getEvent(&event);
    if (isnan(event.relative_humidity)) {
      display.println(F("Error reading humidity!"));
    }
    else {
      display.print(F("H:"));
      display.print(event.relative_humidity);
      display.print(F("%"));
    }
    activeLight = !activeLight;
    display.display();
    delay(delayMS);
  }
}

void drawLoadingScreen(void) {
  display.clearDisplay();
  display.setTextSize(3);             // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE);        // Draw white text
  display.setCursor(0,0);             // Start at top-left corner
  if(!activeLight){
    display.println(F("Make It"));
  }else{
    display.println(F("4 Less"));
  }
  activeLight = !activeLight;
  display.display();
  delay(1000);
}
